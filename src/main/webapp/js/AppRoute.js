/* global loginFormController, angular */

angular
        .module('AppModule', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {

                $routeProvider
                        .when('/landing', {
                            templateUrl: 'templates/myPage.html',
                            controller: 'myAccountController'
                        })
                        .when('/courses', {
                            templateUrl: 'templates/searchCourses.html',
                            controller: 'myAccountController'
                        })
                        .when('/account', {
                            templateUrl: 'templates/account.html',
                            controller: 'accountController'
                        })
                            .when('/search', {
                            templateUrl: 'templates/search.html',
                            controller: 'accountController'
                        })
                        .when('/addCourse', {
                            templateUrl: 'templates/uploadProject.html',
                            controller: 'accountController'
                        })
                        .when('/forum', {
                            templateUrl: 'templates/forum.html',
                            controller: 'forumController'
                        })
                        .when('/landing2', {
                            templateUrl: 'templates/myPage2.html',
                            controller: 'myPage2Controller'
                        })
                        .when('/friends', {
                            templateUrl: 'templates/friends.html',
                            controller: 'friendsController'
                        })
                        .when('/email', {
                            templateUrl: 'https://mail.google.com/mail/u/0/#inbox',
                            controller: ''
                        })
                        
            }]);




