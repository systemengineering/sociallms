angular
        .module('AppModule')
        .controller('friendsController', ['$scope', '$routeParams','authenticationService','$http','$rootScope', function ($scope, $routeParams, authenticationService,$http,$rootScope) {

//         $scope.friends = 'You do not have any friends yet! Search friends!'
            $scope.frshow = false;
            $scope.search = false;
            $scope.friends = [
                {
                    name : 'testProfessor',
                    userType: 'professor',
                    email: 'testProfessor@gmail.com'
                }
                
            ];
            $scope.friends2 = [
                {
                    name : 'testUser',
                    userType: 'student',
                    email: 'testStudent@gmail.com'
                }
                
            ];
            
            $scope.setvizFriends = function() {
                $scope.frshow = true;
                if ($rootScope.student == 'testProfessor'){
                    $scope.friends = [
                {
                    name : 'testUser',
                    userType: 'student',
                    email: 'testStudent@gmail.com'
                }
                
            ];
                }
            }
            
            $scope.setsearchFriends = function(){
                $scope.search = true;
            }
            
            $scope.msg = function(){
                $scope.display = 'No matches for the specified data!';
            }
        }]);