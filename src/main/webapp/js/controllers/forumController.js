angular
        .module('AppModule')
        .controller('forumController', ['$scope', '$routeParams','authenticationService','$http','$rootScope', function ($scope, $routeParams, authenticationService,$http,$rootScope) {

            
        $scope.comment = {
            name: $rootScope.student,
            group: '',
            content: ''
        };

        $http({
                method: 'GET',
                url: 'getForum',
                 })
                .success(function (data, status, headers, config) {
                    $scope.forum = data;
                     console.log(data);

                })
                .error(function (data, status, headers, config) {
                    console.log("Error " + status);
                });

            $scope.send = function() {
                

                    $http({
                        method: 'POST',
                        url: 'forumCtrl',
                        params: {"name": $rootScope.student, "group": $scope.comment.group, "content": $scope.comment.content},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}

                    })
                            .success(function (data, status, headers, config) {
                                
                                        $scope.forum = data;  
                                        $scope.comment = {
                                            name: $rootScope.student,
                                            group: '',
                                            content: ''
                                        };
 
                            })
                            .error(function (data, status, headers, config) {
                                console.log("Error " + status);
                            });

               
            }
    
        }]);


