angular
        .module('AppModule')
        .controller('homeController', ['$scope', '$http', '$window', 'authenticationService','$rootScope','$location', function ($scope, $http, $window, authenticationService,$rootScope,$location) {

                $scope.showLogin = false;
                $scope.showRegister = false;
                $rootScope.showUser = authenticationService.isUserAuthenticated();
                $scope.successTextAlert = "The username and password you entered did not match our records.Please double-check and try again. ";
                $scope.showSuccessAlert = false;
                $scope.user =
                {
                 username : authenticationService.getUser(),
                 password: '',
                 type : ''
                };
                console.log($scope.user.username);
                 // switch flag
                $scope.switchBool = function (value) {
                    $scope[value] = !$scope[value];
                };

                $scope.submit = function () {

                    $http({
                        method: 'POST',
                        url: 'loginCtrl',
                        params: {"username": $scope.user.username, "password": $scope.user.password},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}

                    })
                            .success(function (data, status, headers, config) {
                                if (data.isUserValid === "success")
                                {

                                    $scope.userIsAuthenticated = true;
                                    authenticationService.setUser($scope.user.username, $scope.userIsAuthenticated);
                                    if (data.userRole === "student") {
                                        $scope.userRole = data.userRole;
                                        authenticationService.setUserRole(data.userRole);
                                        $rootScope.showUser = authenticationService.isUserAuthenticated();
                                    }else {
                                        $rootScope.stud = true;
                                        $scope.userRole = data.userRole;
                                        authenticationService.setUserRole(data.userRole);
                                        $rootScope.showUser = authenticationService.isUserAuthenticated();
                                    }
                                    $scope.showLogin = false;
                                } else
                                {
                                    $scope.userIsAuthenticated = "false";
                                    $scope.showSuccessAlert = true;
                                }
                                $rootScope.hideLoginForm = authenticationService.isUserAuthenticated();
                                
                            })
                            .error(function (data, status, headers, config) {
                                console.log("Error " + status);
                            });

                };
                $scope.login = function (){
                    $scope.showLogin = true;
                }
                $scope.registerModal = function (){
                    $scope.showLogin = false;
                    $scope.showRegister = true;
                }

            }])
           .directive('modal', function () {
            return {
                template: '<div class="modal fade">' +
                        '<div class="modal-dialog">' +
                        '<div class="modal-content">' +
                        '<div class="modal-header">' +
                        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                        '<h4 class="modal-title">{{ title }}</h4>' +
                        '</div>' +
                        '<div class="modal-body" ng-transclude></div>' +
                        '</div>' +
                        '</div>' +
                        '</div>',
                restrict: 'E',
                transclude: true,
                replace: true,
                scope: true,
                link: function postLink(scope, element, attrs) {
                    scope.title = attrs.title;

                    scope.$watch(attrs.visible, function (value) {
                        if (value == true)
                            $(element).modal('show');
                        else
                            $(element).modal('hide');
                    });

                    $(element).on('shown.bs.modal', function () {
                        scope.$apply(function () {
                            scope.$parent[attrs.visible] = true;
                        });
                    });

                    $(element).on('hidden.bs.modal', function () {
                        scope.$apply(function () {
                            scope.$parent[attrs.visible] = false;
                        });
                    });
                }
            };
        });

    







