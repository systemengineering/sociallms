angular
        .module('AppModule')
        .controller('RegisterController', ['$scope', '$http', '$window', 'authenticationService', function ($scope, $http, $window, authenticationService) {

                $scope.register = function () {

                    $http({
                        method: 'POST',
                        url: 'loginRegister',
                        params: {"username": $scope.username, "password": $scope.password, "type": $scope.type},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}

                    })
                            .success(function (data, status, headers, config) {
                                if (data.isUserValid === "success")
                                {
                                    $window.location.href = '#/login';

                                } 

                            })
                            .error(function (data, status, headers, config) {
                                console.log("Error " + status);
                            });

                };



            }]);




