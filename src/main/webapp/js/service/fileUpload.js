angular
        .module('AppModule')
        .service('fileUpload', ['$http','$window', function ($http, $window) {
                this.uploadFileToUrl = function (file, note, category, uploadUrl) {

                    var fd = new FormData();
                    fd.append('upfile', file);
                    fd.append('note', note);
                    fd.append('category', category);
                    $http.post(uploadUrl, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    })
                            .success(function (data) {
                                console.log(data);
                               
                                $window.location.href = '#/uploadSuccess';
                            })
                            .error(function () {
                            });
                };
            }]);
