/* global angular */
angular
        .module('AppModule')
        .service('authenticationService', function () {
            var _user = null ;
            var _isUserAuthenticated = false;
            var _userRole = 'proposer';
            
           
            this.getUser = function () {
                    return _user;
            },
            this.isUserAuthenticated = function () {
                    return _isUserAuthenticated;
            },
            this.getUserRole = function (){
                    return _userRole;
            },
            this.setUser = function (user_new, value) {
                    _user = user_new;
                    _isUserAuthenticated = value;
            },
            this.setUserRole = function (userRole) {
                    _userRole = userRole;
            };
        });


