/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.daoImpl;

import com.sociallms.dao.LessonsRepository;
import com.sociallms.model.Lesson;
import com.sociallms.model.Message;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Silvia
 */
@Repository(value = "lessonDao")
@Transactional
public class LessonDaoImpl implements LessonsRepository {

    @Autowired
    SessionFactory sessionFact;

    @Override
    public boolean insertLesson(Lesson l) {
        if (l == null) {
            return false;
        }
        return (sessionFact.getCurrentSession().save(l) != null);
    }

    @Override
    public List<Lesson> findAllLessons() {
        return sessionFact.getCurrentSession().createQuery("from Lesson l").list();
    }

}
