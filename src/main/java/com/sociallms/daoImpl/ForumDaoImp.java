/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.daoImpl;

import com.sociallms.dao.ForumRepository;
import com.sociallms.model.Forum;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Silvia
 */
@Repository(value = "forumDao")
@Transactional
public class ForumDaoImp implements ForumRepository{
    @Autowired
    SessionFactory sessionFact;

    @Override
    public boolean insertForum(Forum c) {
         if (c == null) {
            return false;
        }
        return (sessionFact.getCurrentSession().save(c) != null);
    }

    @Override
    public List<Forum> findAllForums() {
        return sessionFact.getCurrentSession().createQuery("from Forum f").list();
    }
}
