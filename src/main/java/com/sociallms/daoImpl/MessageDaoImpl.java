/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.daoImpl;

import com.sociallms.dao.MessageRepository;
import com.sociallms.model.Message;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Silvia
 */
@Repository(value = "messageDao")
@Transactional
public class MessageDaoImpl implements MessageRepository{
    
    @Autowired
    SessionFactory sessionFact;
    
    @Override
    public boolean insertMessage(Message message) {
        if (message == null) {
            return false;
        }
        return (sessionFact.getCurrentSession().save(message) != null);
    }

    @Override
    public List<Message> findAllMessages() {
       return sessionFact.getCurrentSession().createQuery("from Message m").list();
    }
    
}
