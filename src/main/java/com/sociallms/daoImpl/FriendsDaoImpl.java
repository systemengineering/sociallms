/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.daoImpl;

import com.sociallms.dao.FriendsRepository;
import com.sociallms.model.Friends;
import java.util.List;
import java.util.Set;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Silvia
 */
@Repository(value = "friendsDao")
@Transactional
public class FriendsDaoImpl implements FriendsRepository{

    @Autowired
    SessionFactory sessionFact;
    
    @Override
    public boolean insertFriend(Friends f) {
         if (f == null) {
            return false;
        }
        return (sessionFact.getCurrentSession().save(f) != null);
    }

    @Override
    public Set<Friends>  getFriendsByUser(Long user) {
        
        String hql = "from Friends f where f.user = ?";
        List result = sessionFact.getCurrentSession().createQuery(hql)
                .setLong(0, user)
                .list();
        
        if(!result.isEmpty()) {
            return (Set<Friends>) result;
        }
        return null;
    }

    @Override
    public List<Friends> findAllFriends() {
        return sessionFact.getCurrentSession().createQuery("from Friends f").list();
    }
    
}
