/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.daoImpl;

import com.sociallms.dao.UserRepository;
import com.sociallms.model.User;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Silvia
 */
@Repository(value = "userDao")
@Transactional
public class UserDaoImpl implements UserRepository {

    @Autowired
    SessionFactory sessionFact;

    @Override
    public boolean insertUser(User user) {
        if (user == null) {
            return false;
        }

        if (user.getEmailAddress() == null || user.getPassword() == null) {
            return false;
        }

        return (sessionFact.getCurrentSession().save(user) != null);

    }

    @Override
    public List<User> findAllUsers() {
        return sessionFact.getCurrentSession().createQuery("from User u").list();

    }

    @Override
    public User getUserByEmailAndPassword(String email, String pass) {
        String hql = "from User u where u.emailAddress = ? and u.password = ?";
        List result = sessionFact.getCurrentSession().createQuery(hql)
                .setString(0, email)
                .setString(1, pass)
                .list();
        
        if(!result.isEmpty()) {
            User u = (User) result.get(0);
            return u;
        }
        return null;
    }

}
