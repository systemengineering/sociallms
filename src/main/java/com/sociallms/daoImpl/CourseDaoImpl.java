/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.daoImpl;

import com.sociallms.dao.CoursesRepository;
import com.sociallms.model.Course;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Silvia
 */
@Repository(value = "courseDao")
@Transactional
public class CourseDaoImpl implements CoursesRepository{
    
     @Autowired
    SessionFactory sessionFact;

    @Override
    public boolean insertCourse(Course c) {
       if (c == null) {
            return false;
        }
        return (sessionFact.getCurrentSession().save(c) != null);
    }

    @Override
    public List<Course> findAllCourses() {
        return sessionFact.getCurrentSession().createQuery("from Course c").list();
    }
   
}
