/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.controller;

import com.sociallms.model.Message;
import com.sociallms.service.UtilService;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xml.sax.SAXException;

/**
 *
 * @author Silvia
 */
@Controller
@RequestMapping(value = "/")
public class UtilController {
    
    @Autowired
    UtilService utilService;

    @RequestMapping(value = "messageCtrl", method = RequestMethod.POST)
    public @ResponseBody
    String messageController(HttpServletRequest request, @RequestParam("name") String name,
            @RequestParam("email") String email,@RequestParam("phone") String phone,@RequestParam("message") String message) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

            Message msg = new Message();
            msg.setName(name);
            msg.setEmail(email);
            msg.setPhone(phone);
            msg.setMessage(message);
            utilService.insertMessageIntoDatabase(msg);
            return "{\"isMessageValid\" : \"success\"}";
    }
    
   
}
