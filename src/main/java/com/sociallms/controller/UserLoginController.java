package com.sociallms.controller;
import com.sociallms.model.Course;
import com.sociallms.model.Forum;
import com.sociallms.model.User;
import com.sociallms.service.UserService;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xml.sax.SAXException;


@Controller
@RequestMapping(value = "/")
public class UserLoginController {
 
    @Autowired
    UserService userService;

    @RequestMapping(value = "loginCtrl", method = RequestMethod.POST)
    public @ResponseBody
    String loginController(HttpServletRequest request, @RequestParam("username") String username,
            @RequestParam("password") String password) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
            
            User u = new User();
            u = userService.getUserByEmailAndPasswordFromDatabase(username, password);
            if (u != null){
                return "{\"isUserValid\" : \"success\", \"userRole\" :\""+u.getType()+ "\" }";
            }
            else{
                return "{\"isUserValid\" : \"fail\"}";
            }
       
    }
    @RequestMapping(value = "getCoursesCtrl", method = RequestMethod.POST)
    public @ResponseBody Course getCourses(HttpServletRequest request, @RequestParam("username") String username,
            @RequestParam("password") String password){
        
            User u = new User();
            u = userService.getUserByEmailAndPasswordFromDatabase(username, password);
            List<Course> allCoursesFromDatabase = userService.getAllCoursesFromDatabase();
            Course returnC = new Course();
            for (Course c : allCoursesFromDatabase)
            {
                returnC.setName(c.getName());
                returnC.setDomain(c.getDomain());
                returnC.setDuration(c.getDuration());
            }
           
            if (u != null ){
                return returnC;
            }
            else{
                return null;
            }
        
    }
    @RequestMapping(value = "getForum", method = RequestMethod.GET)
    public @ResponseBody Set<Forum> getForum(){
        
            
            List<Forum> allForumsFromDatabase = userService.getAllForumsFromDatabase();
            
           
            if (allForumsFromDatabase != null ){
                return new HashSet<>(allForumsFromDatabase);
            }
            else{
                return null;
            }
        
    }
    
    @RequestMapping(value = "forumCtrl", method = RequestMethod.POST)
    public @ResponseBody
    Set<Forum> forumController(HttpServletRequest request, @RequestParam("name") String name,
            @RequestParam("group") String group,@RequestParam("content") String content){

            Forum f = new Forum();
            f.setName(name);
            f.setGroup(group);
            f.setContent(content);
            userService.insertForumIntoDatabase(f);
            List<Forum> allForumsFromDatabase = userService.getAllForumsFromDatabase();
            
           
            if (allForumsFromDatabase != null ){
                return new HashSet<>(allForumsFromDatabase);
            }
            else{
                return null;
            }
    }

   
}
