/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.dao;

import com.sociallms.model.Lesson;
import java.util.List;

/**
 *
 * @author Silvia
 */
public interface LessonsRepository {
      
     public boolean insertLesson(Lesson c);
      public List<Lesson> findAllLessons();
}
