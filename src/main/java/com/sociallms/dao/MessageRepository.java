/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.dao;

import com.sociallms.model.Message;
import java.util.List;

/**
 *
 * @author Silvia
 */
public interface MessageRepository {
    
    public boolean insertMessage(Message message);
    public List<Message> findAllMessages();
}
