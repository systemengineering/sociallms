/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.dao;

import com.sociallms.model.Forum;
import java.util.List;

/**
 *
 * @author Silvia
 */
public interface ForumRepository {
      public boolean insertForum(Forum c);
     public List<Forum> findAllForums();
}
