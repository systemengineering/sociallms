/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.dao;
import com.sociallms.model.User;
import java.util.List;

public interface UserRepository {

    public boolean insertUser(User user);

    public  User getUserByEmailAndPassword(String email, String pass);

    public List<User> findAllUsers();
       
}
