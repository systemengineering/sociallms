/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.dao;

import com.sociallms.model.Friends;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Silvia
 */
public interface FriendsRepository {
       
    public boolean insertFriend(Friends f);

    public  Set<Friends> getFriendsByUser(Long user);

    public List<Friends> findAllFriends();
}
