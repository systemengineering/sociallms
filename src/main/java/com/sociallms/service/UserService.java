/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.service;
import com.sociallms.dao.CoursesRepository;
import com.sociallms.dao.ForumRepository;
import com.sociallms.dao.FriendsRepository;
import com.sociallms.dao.UserRepository;
import com.sociallms.model.Course;
import com.sociallms.model.Forum;
import com.sociallms.model.Friends;
import com.sociallms.model.User;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepo;
    
    @Autowired
    CoursesRepository courseRepo;
    
    @Autowired
    ForumRepository forumRepo;
    
    @Autowired
    FriendsRepository friendsRepo;

    public void insertUserIntoDatabase(User user) {
        userRepo.insertUser(user);
    }
    public void insertFriend(Friends f){
        friendsRepo.insertFriend(f);
    }

    public  Set<Friends> getFriendsByUser(Long user){
      
        return friendsRepo.getFriendsByUser(user);
    
    }

    public List<Friends> findAllFriends(){
        return friendsRepo.findAllFriends();
    }
    
    public void insertCourseIntoDatabase(Course c) {
        courseRepo.insertCourse(c);
    }
    public void insertForumIntoDatabase(Forum c) {
        forumRepo.insertForum(c);
    }

    public User getUserByEmailAndPasswordFromDatabase(String email, String pass) {
        return userRepo.getUserByEmailAndPassword(email, pass);

    }

    public List<User> getAllUsersFromDatabase() {
        return userRepo.findAllUsers();
    }
    
     public List<Course> getAllCoursesFromDatabase() {
        return courseRepo.findAllCourses();
    }
      public List<Forum> getAllForumsFromDatabase() {
        return forumRepo.findAllForums();
    }
     
    public int getUserIdFromDatabaseByUsernameAndPassword(String username, String password) {

        List<User> lst = userRepo.findAllUsers();
        int id= 0;
        for (User u : lst) {
            if(username.equals(u.getEmailAddress()) && password.equals(u.getPassword()))
            {
                id=u.getIdUser();
            }
        }
         return id;

    }
    
}

