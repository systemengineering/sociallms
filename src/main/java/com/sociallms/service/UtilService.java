/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sociallms.service;

import com.sociallms.dao.CoursesRepository;
import com.sociallms.dao.MessageRepository;
import com.sociallms.model.Course;
import com.sociallms.model.Message;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Silvia
 */
@Service
public class UtilService {
    
    @Autowired
    MessageRepository messageRepo;
    @Autowired
    CoursesRepository courseRepo;
    
     public void insertMessageIntoDatabase(Message message) {
        messageRepo.insertMessage(message);
    }

    public List<Message> getAllMessagesFromDatabase() {
        return messageRepo.findAllMessages();
    }

    public void insertCoursesIntoDatabase(Course c) {
        courseRepo.insertCourse(c);
    }

    public List<Course> getAllCoursesFromDatabase() {
        return courseRepo.findAllCourses();
    }
}
